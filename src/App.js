import { Provider } from 'react-redux';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import AuthPage from './components/pages/authPage/AuthPage';
import Basket from './components/pages/BasketPage/BasketPage';
import ProductDescription from './components/pages/ProductDescription/ProductDescription';
import Products from './components/pages/productsPage/ProductPage';
import { store } from './components/store/store';



function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
      <Routes>
        <Route path="/" element={<AuthPage signIn />} />
        <Route path="/signUp" element={<AuthPage signUp />} />
        <Route path="/products" element={<Products />} />
        <Route path="/product/:productId" element={<ProductDescription />} />
        <Route path="/basket" element={<Basket />} />
      </Routes>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
