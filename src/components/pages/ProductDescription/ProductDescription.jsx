import LogoutButton from '../../allButtons/LogoutButton/LogoutButton';
import BackButton from '../../allButtons/BackButton/BackButton';
import classes from './ProductDescription.module.scss'
import { Link, useParams } from 'react-router-dom';
import { productList } from '../productsPage/products/products';
import { useDispatch, useSelector } from 'react-redux';
import BasketButton from '../../allButtons/BasketButton/BasketButton';
import { addToBasket } from '../../store/states/basket';

export default function ProductDescription() {
  const {productId} = useParams();
  const cart = useSelector((state) => state.basket)
  const dispatch = useDispatch();
  const product = productList.find(e => e.id === Number(productId));
  const AddToCart = () => {
    dispatch(addToBasket(product))
  }

  const cartTotalPrice = cart.reduce((totalPrice, product) => totalPrice + product.price, 0);


  return (
    <div>
      <header className={classes.header}>
        <Link to={"/products"}>
          <BackButton/>
        </Link>
        <div className={classes.header__basket}>
            {cart.length} товара <br/> на сумму {cartTotalPrice} ₽
            <Link to={"/basket"}>
            <BasketButton/>
            </Link>
        </div>
        <Link to={"/signIn"} className={classes.header__button}>
          <LogoutButton/>
        </Link>
      </header>
      <main className={classes.main}>
        <div>
          <img className={classes.main__image} src={product.image} alt="" />
        </div>
        <div className={classes.main__content}>
          <h1 className={classes.main__title}>
            {product.title}
          </h1>
        <p className={classes.main__text}>
          {product.fulltext}
        </p>
        <div className={classes.footer}>
        <span className={classes.main__price}>
          {product.price} ₽
        </span>
        <span>  /  </span>
        {Boolean(product.weight) && <span className={classes.main__weight}>{product.weight} г.</span>}
        {Boolean(product.amount) && <span className={classes.main__weight}>{product.amount} шт.</span>}
        <button onClick={AddToCart} className={classes.main__button}>
          В корзину
        </button>
        </div>
        </div>
      </main>
    </div>
  )  
}