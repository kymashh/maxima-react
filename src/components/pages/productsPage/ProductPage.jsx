import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import BasketButton from '../../allButtons/BasketButton/BasketButton';
import LogoutButton from '../../allButtons/LogoutButton/LogoutButton';
import ProductCard from '../../productCard/productCard';
import { addToBasket } from '../../store/states/basket';
import './ProductPage.scss';
import {productList} from './products/products';

function Products() {
  const cart = useSelector((state) => state.basket)
  const dispatch = useDispatch();

  const cartTotalPrice = cart.reduce((totalPrice, product) => totalPrice + product.price, 0);

  const AddToCart = (product) => {
    dispatch(addToBasket(product))
  }

  return (
  <div>
    <header className='header'>
      <h1 className='header__title'>наша продукция</h1>
      <div className='header__cart'>
        <div>
          {cart.length} товара <br/> на сумму {cartTotalPrice} ₽
        </div>
      </div>
      <Link to={"/basket"}>
        <BasketButton/>
      </Link>
      <Link to={"/signIn"} className='header__button'>
      <LogoutButton/>
      </Link>
    </header>
    <main className='product-list-container'>
      {productList.map(product => (
        <div key={product.id} className='product-list-item'>
          <ProductCard
              id={product.id}
              title={product.title}
              text={product.text}
              image={product.image}
              price={product.price}
              weight={product.weight}
              amount={product.amount}
              onButtonClick={() => AddToCart(product)}
          />
        </div>
      ))}
    </main>
  </div>
  )
}

export default Products