import Form from '../../FORM/Form'
import './AuthPage.scss'

function AuthPage({signIn, signUp}) {
  return (
    <div className='auth-page'>
      {signIn && <Form
        linkText={'Зарегестрироваться'}
        linkTo="/signUp"
        header={'ВХОД'}
        buttonText={'Войти'}
      />}
      {signUp && <Form
        linkText={'Авторизоваться'}
        linkTo="/signIn"
        header={'РЕГИСТРАЦИЯ'}
        buttonText={'Зарегистрироваться'}
      />}
    </div>
  )
}

export default AuthPage