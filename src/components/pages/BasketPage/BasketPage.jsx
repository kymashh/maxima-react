import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import BackButton from "../../allButtons/BackButton/BackButton";
import Button from "../../allButtons/buttons/Buttons";
import LogoutButton from "../../allButtons/LogoutButton/LogoutButton";
import RemoveButton from "../../allButtons/RemoveButton/RemoveButton";
import { removeFromBasket } from "../../store/states/basket";
import './BasketPage.scss'


export default function Basket() {
  const cart = useSelector((state) => state.basket)
  const cartTotalPrice = cart.reduce((totalPrice, product) => totalPrice + product.price, 0);
  const dispatch = useDispatch()
  const removeFromCart = (id) => {
    dispatch(removeFromBasket(id));
  }
  return (
    <div>
      <header className="header">
        <Link to={"/products"} className="header__back">
          <BackButton/>
        </Link>
        <span className="header__title_basket">
          Корзина с выбранными товарами
        </span>
        <Link to={"/signIn"} className="header__logout">
          <LogoutButton/>
        </Link>
      </header>
      <main className="main">
      {cart.map((product, index) => (
        <div key={index} className="main__content">
            <img className="main__content_image" src={product.image} alt=""/>
            <span className="main__content_title">{product.title}</span>
            <span className="main__content_price">{product.price} ₽</span>
            <RemoveButton onClick={() => removeFromCart(product.basket_id)}/>
        </div>))}
      </main>
      <footer className="footer">
        <img className="footer__image" src="./Vector 30.png" alt="" />
        <span className="footer__text">Заказ на сумму: </span>
        <span className="footer__price">{cartTotalPrice} ₽</span>
        <div className="footer__button">
        <Button>Оформить Заказ</Button>
        </div>
      </footer>
    </div>
  )
}