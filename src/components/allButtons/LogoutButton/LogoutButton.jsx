import './LogoutButton.scss';

export default function LogoutButton() {
  return (
    <button className='lbutton'>
      Выйти
    </button>
  )
}