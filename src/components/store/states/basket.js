import { createSlice } from '@reduxjs/toolkit'
import { nanoid } from 'nanoid'

const basket_state = 'basket_state'
const initialBasket = localStorage.getItem(basket_state);
const initialState = initialBasket ? JSON.parse(initialBasket) : [];

export const basketSlice = createSlice({
  name: 'basket',
  initialState,
  reducers: {
    addToBasket: (state, action) => {
      const product = {
        ...action.payload,
        basket_id : nanoid()
      }
      state.push(product);
      localStorage.setItem(basket_state, JSON.stringify(state));
    },
    removeFromBasket: (state, action) => {
      console.log(action.payload)
      const newState = state.filter((product) => product.basket_id !== action.payload)
      localStorage.setItem(basket_state, JSON.stringify(newState));
      return newState;
    }
  }
})

export const { addToBasket, removeFromBasket } = basketSlice.actions;
export default basketSlice.reducer;