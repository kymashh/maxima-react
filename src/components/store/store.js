import { configureStore } from '@reduxjs/toolkit'
import basket from './states/basket'

export const store = configureStore({
  reducer: {
    basket
  },
})
