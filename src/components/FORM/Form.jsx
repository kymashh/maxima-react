import Button from '../allButtons/buttons/Buttons';
import './Form.scss';
import { Link } from "react-router-dom";

function Form({header, buttonText, linkText, linkTo}) {
   return (
   <form className="field" id="form">
      <fieldset className="field__border">
         <div className='field__border__regis'>
            <Link to={linkTo} className='field__border__a'>
            {linkText}
            </Link>
         </div>
         <div className="field__border__area-first">
            <h1 className="field__border__text">{header}</h1>
            <input className="field__border__line" type={'text'} placeholder="  Логин"></input>
         </div>
         <div className="field__border__area-second">
            <legend className="field__border__text"></legend>
            <input className="field__border__line" placeholder="  Пароль" type={'password'}></input>
         </div>
         <div className='field__border__check'>
            <input type="checkbox" className='field__border__checkbox'></input>
            <span className='field__border__checkbox-text'>Я согласен получать обновления на почту</span>
         </div>
         <Link to={"/products"} className='field__border__but'>
            <Button>{buttonText}</Button>
         </Link>
      </fieldset>
   </form>
   );
}

export default Form;