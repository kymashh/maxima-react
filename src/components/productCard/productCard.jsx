import { Link } from 'react-router-dom';
import CircleButton from '../allButtons/CircleButton/CircleButton';
import './productCard.scss';


export default function ProductCard({
  id,
  image,
  title,
  text,
  price, 
  weight,
  amount,
  onButtonClick,
}) {
  return (
    <div className='product-card'>
      <Link to={`/product/${id}`}>
      <img className='product-card__image' src={image} alt="" />
      </Link>
      <span className='product-card__title'>{title}</span>
      <span className='product-card__text-w-a'>{text}</span>
      <div className='product-card__price-block'>
        <span className='product-card__price'>{price} ₽</span>
        <span> / </span>
        {Boolean(weight) ? <span className='product-card__text-w-a'>{weight} г.</span> : <span className='product-card__text-w-a'>{amount} шт.</span>}
      </div>
      <div className='product-card__button'>
        <CircleButton onClick={onButtonClick}/>
      </div>
    </div> 
  )
}